\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{amsmath} % allows you to put text in the math environment.
\usepackage{bm}
\usepackage[margin=0.7in]{geometry}
\usepackage{tikz}
\usepackage{tikz-3dplot}
\usetikzlibrary{shapes, calc, positioning}
\tdplotsetmaincoords{70}{120}

\title{\Huge\textbf{Project 1}}
\author{Amer Bitar and James Koch} %share with Da Yuan: dayu@student.chalmers.se
\date{\oldstylenums{2019}-\oldstylenums{01}-\oldstylenums{24}}

% --------------------Tikz
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{calc}
\usetikzlibrary{patterns}

% --------------------Graphics Path
\graphicspath{ {./imgs/} }

% --------------------Indention
\setlength{\parindent}{0cm}
\setlength{\parskip}{\baselineskip}

% --------------------Colors
\usepackage{color} %red & green & blue & yellow & cyan & magenta & black & white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red & Green & Blue
\definecolor{mylilas}{RGB}{170,55,241}

\begin{document}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

%--------------------Title Page
\maketitle
\newpage % to create a title page

\section{Problem Description} \label{sec:section1}
The problem to be solved is to determine the force required to open a tube for a total slit width of \(6mm\).
The geometry of the problem is shown in Figure~\ref{fig:problemgeometry}.
Furthermore, the outer and inner diameters of the tube are \(0.5m\) and \(0.3m\), respectively.
The tube is considered as 2-dimensional for applying the finite element method but has a thickness of \(0.5m\).

\begin{figure}[!h]
    \centering
    \includegraphics[scale=0.5]{imgs/problemgeometry.png}
    \caption{The problem geometry.}
    \label{fig:problemgeometry}
\end{figure}

The tube is considered to be made of a linear elastic material having a elastic modulus of \(210GPa\) and a Poisson's ratio of \(0.3\).
Additionally, the plane strain condition is assumed during the solution by the finite element method.

The geometry also allows for the symmetry about the slit opening to be utilized (dashed line in Figure~\ref{fig:problemgeometry}) to decrease the problem domain to reduce the computational time needed.

\section{Method Description}

\subsection{Strong formulation}
The strong form can be stated as follows in Equation~\ref{eq-strongform} using Voigt notation.
For the problem defined above in section \ref{sec:section1}, there will exist 4 boundary conditions of which are denoted as B1, B2, B3, and B4.
The prescribed displacement is applied on boundary, B1, in the y-direction while the symmetrical boundary condition is denoted as boundary, B3.

\begin{equation} \label{eq-strongform}
    \begin{cases}
      - \tilde{\nabla}^T \boldsymbol{\sigma}  &= 0 \text{ in the domain } A \\
      u_y &= -0.003 \text{ on } \Gamma_{g,1} \\
      t_x &= 0 \text{ on } \Gamma_{h,1} \\
      u_y &= 0 \text{ on } \Gamma_{g,3} \\
      t_x &= 0 \text{ on } \Gamma_{h,3} \\
      \mathbf{t} = \sigma_n &= 0 \text{ on } \Gamma_2 \text{ and } \Gamma_4 
    \end{cases}
\end{equation}

\subsection{Weak formulation}
The weak form can be derived from the strong form (Eq.~\ref{eq-strongform}) by multiplying the equation with a weight function, \(\mathbf{v} = v(x, y)\), and integrating over the domain, \(A\).

\begin{equation} \label{eq-weakform1}
  \centering
  \int_A \mathbf{v} \tilde{\nabla}^T \boldsymbol{\sigma} t dA = 0
\end{equation}

The next step (Equation~\ref{eq-weakform2}) is to integrate by parts by utilizing the Green-Gauss theorem.

\begin{equation} \label{eq-weakform2}
  \centering
    - \int_A (\tilde{\nabla} \mathbf{v})^T \boldsymbol{\sigma} t dA + \int_{\Gamma} \mathbf{n}^T (\mathbf{v} \boldsymbol{\sigma}) t d\Gamma = 0
\end{equation}

Substituting the constitutive equation of Hooke's law for linear elasticity, \(\sigma = \mathbf{D} \epsilon\), where \(\epsilon = \tilde{\nabla} \mathbf{u}\) and implementing both Dirichlet (essential) and Neumann (natural) boundary conditions, the weak form will take the form as follows in Equation~\ref{eq-weakform3}.
It is important to note that \(t\) is the thickness of the 2-dimensional domain while \(\mathbf{t}\) is the traction vector along the Dirichlet boundary.
On the other hand, \(h\) is a vector-valued variable denoting the stress condition on the Neumann boundary.

\begin{equation} \label{eq-weakform3}
  \centering
  \begin{cases}
    \int_A (\tilde{\nabla} \mathbf{v})^T \tilde{\nabla} \mathbf{u} dA &= \int_{\Gamma_{g}} \mathbf{v}^T \mathbf{t} t d\Gamma + \int_{\Gamma_{h}} \mathbf{v}^T h t d\Gamma \\
    u_y &= -0.003 \text{ on } \Gamma_{g,1} \\
    u_y &= 0 \text{ on } \Gamma_{g,3}
  \end{cases}
\end{equation}

\subsection{Finite element formulation}
To derive the FE-form it is necessary to introduce the approximations for the displacement in terms of nodal degrees of freedom and shape functions.
It is assumed in this derivation that linear shape functions will be used over rectangular isoparametric elements.
The approximation for \(\mathbf{u}\), \(\tilde{\nabla} \mathbf{u}\), \(\mathbf{v}\), \(\tilde{\nabla} \mathbf{v}\) are shown in Equation~\ref{eq-FEapproximations}.

\begin{align} \label{eq-FEapproximations}
  \centering
  \mathbf{u} &\approx \mathbf{N} \mathbf{a} \\
  \tilde{\nabla} \mathbf{u} &\approx \mathbf{B} \mathbf{a} \\
  \mathbf{v} &\approx \mathbf{N} \mathbf{c} = \mathbf{c}^T \mathbf{N}^T \\
  (\tilde{\nabla} \mathbf{v})^T &\approx \mathbf{B} \mathbf{c} = \mathbf{c}^T \mathbf{B}^T
\end{align}

In the equation above (Eq.~\ref{eq-FEapproximations}), \(\mathbf{a}\) represents the nodal displacements in both dimensions, \(\mathbf{c}\) is a vector of arbitrary coefficients and \(\mathbf{N}\) and \(\mathbf{B}\) represent the shape functions and the derivatives of the shape functions, respectively which are shown in Equation~\ref{eq-shapefunctions}.

\begin{align} \label{eq-shapefunctions}
  \mathbf{N} &= \begin{bmatrix}
    N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4 & 0 \\
    0 & N_1 & 0 & N_2 & 0 & N_3 & 0 & N_4
  \end{bmatrix} \\
  \mathbf{B} &= \begin{bmatrix}
    \frac{\partial N_1}{\partial x} & 0 & \frac{\partial N_2}{\partial x} & 0 & \frac{\partial N_3}{\partial x} & 0 & \frac{\partial N_4}{\partial x} \\
    0 & \frac{\partial N_1}{\partial y} & 0 & \frac{\partial N_2}{\partial y} & 0 & \frac{\partial N_3}{\partial y} & 0 & \frac{\partial N_4}{\partial y} \\
    \frac{\partial N_1}{\partial y} & \frac{\partial N_1}{\partial x} & \frac{\partial N_2}{\partial y} & \frac{\partial N_2}{\partial x} & \frac{\partial N_3}{\partial y} & \frac{\partial N_3}{\partial x} & \frac{\partial N_4}{\partial y} & \frac{\partial N_4}{\partial x}
  \end{bmatrix}
\end{align}

Inserting the approximations from Equation~\ref{eq-FEapproximations} into the weak form (Eq.~\ref{eq-weakform3}, the resulting FE-form is shown below in Equation~\ref{eq-FEform}.

\begin{align} \label{eq-FEform}
  \centering
  &\mathbf{c}^T \int_A \mathbf{B}^T \mathbf{D} \mathbf{B} dA \mathbf{a} = \mathbf{c}^T \left[ \int_{\Gamma_{g}} \mathbf{N}^T \mathbf{t} t d\Gamma + \int_{\Gamma_{h}} \mathbf{N}^T h t d\Gamma \right] \\
  &\begin{cases}
    \mathbf{K} \mathbf{a} &= \mathbf{f_b} \\
    u_y &= -0.003 \text{ on } \Gamma_{g,1} \\
    u_y &= 0 \text{ on } \Gamma_{g,3}    
  \end{cases}
\end{align}

In this case since this problem is classified as a linear boundary value problem (linear elasticity), it is not necessary to re-write the FE-form into the form usually used for non-linear problems; however, for non-linear problems the FE-form is generally written as \(\mathbf{f_{int}}(\mathbf{a}) = \mathbf{f}\).

\subsubsection{Implementation in MATLAB}
The implementation of Eq.~\ref{eq-FEform} requires that the stiffness matrix is computed which can be done as in the following lines of code.
It is important to note that by utilizing the MATLAB function \lstinline{spalloc} for sparse matrices, a significant speed up of the code can be achieved.

\begin{lstlisting}
% initialize matrices
N = 20;
K = spalloc(ndof, ndof, N*ndof);
f = zeros(size(K, 1), 1);

% define inputs
ue = zeros(1, 8)';
ep = [2, thickness, 2, 1]; 
mp = [E, nu];

n = 9;
% for loop
for i = 1:size(Edof, 1)
    [Ke, fint, fext, ~] = elem4n(ue, Ex(i,:), Ey(i,:), ep, mp);
    K(Edof(i, 2:n), Edof(i, 2:n)) = K(Edof(i, 2:n), Edof(i, 2:n)) + Ke;
    f(Edof(i, 2:n), 1) = f(Edof(i, 2:n), 1) + (fint + fext);
end
\end{lstlisting}

After the computation of the stiffness matrix, the boundary conditions need to be set (see derivations in the previous sections of the report) before the solution can be computed since in this instance Eq.~\ref{eq-FEform} is linear FE problem.
The solution is obtained using the method of partitioning a system of equations into free and constrained degrees of freedom which is further explained in the next section.

The boundary conditions are then set according to those shown above in the mathematical FE derivations which are implemented as follows.

\begin{lstlisting}
%% define the Essential BCs
% symmetry boundary -> B3
bc3 = [B3(1, 1), 0;
       B3(:, 2), zeros(size(B3, 1), 1)];

% prescribed displacement boundary -> B1
bc1 = [B1(:, 2), delta.*ones(size(B1, 1), 1)];
%      B1(:, 1), zeros(size(B1, 1), 1);

bc = [bc1; bc3];
\end{lstlisting}

Furthermore, the solution to the system of equations in Equation~\ref{eq-FEform} is implemented in the following code.

\begin{lstlisting}
function [a, Q] = mysolveq(K, f, bc)
%mysolveq

% define free dofs based on which dofs are constrained (i.e. take the
% remaining dofs from total list of dofs
fd = (1:size(K, 1))';
fd(bc(:, 1)) = [];

% partition the stiffness matrix
Kcc = K(bc(:, 1), bc(:,1));  % corresponding to constrained dofs
Kff = K(fd(:, 1), fd(:, 1)); % correspondint to free dofs
Kfc = K(fd(:, 1), bc(:, 1));
Kcf = Kfc';

% define the known displacements
g = bc(:, 2);

% define the known forces
f = f(fd(:, 1));

% solve the system of equations as per the partioned matrix
d = Kff\(f-Kfc*g);
r = Kcc*g + Kcf*d;

a = zeros(size(K, 1), 1);
a(fd(:,1)) = d;
a(bc(:,1)) = bc(:,2);
Q = zeros(size(K, 1), 1);
Q(bc(:,1)) = r;

end
\end{lstlisting}

\subsubsection{Partitioning of the FE system of linear equations}
To solve the system of equations presented in Equation~\ref{eq-FEform}, so-called partitioning of the system is performed to divide the unknown and known nodal degrees of freedom.
The partitioned system of equations is presented in Equation~\ref{eq-partitionedFEsystem}, where the subscripts F and C denote the free and constrained nodal degrees of freedom.

\begin{align} \label{eq-partitionedFEsystem}
  \begin{bmatrix}
    \mathbf{K}_{FF} & \mathbf{K}_{FC} \\
    \mathbf{K}_{CF} & \mathbf{K}_{CC}
  \end{bmatrix}
             \begin{bmatrix}
               \mathbf{a}_F \\
               \mathbf{a}_C
             \end{bmatrix}
  &=
    \begin{bmatrix}
      \mathbf{f}_F \\
      \mathbf{f}_C
    \end{bmatrix}
\end{align}

From the above equation (Eq.~\ref{eq-partitionedFEsystem}), two separate equations with only two unknowns is generated whereby the top equation is solvable for the unknown nodal displacements, \(\mathbf{a}_F\), as equal to \(\mathbf{K}_{FF}^{-1}(\mathbf{K}_{FC} \mathbf{a}_C)\) since \(\mathbf{f}_F = 0\) for this problem due to zero external body load.
Once the top equation has been solved, it is then possible to use the resulting nodal displacements, \(\mathbf{a}_F\), to calculate the unknown reaction forces, defined by \(\mathbf{f}_C\) by the Equation~\ref{eq-bottompartitionedequation}.

\begin{align} \label{eq-bottompartitionedequation}
  \mathbf{K}_{CF} \mathbf{a}_F + \mathbf{K}_{CC} \mathbf{a}_C &= \mathbf{f}_C \\
  \mathbf{K}_{CF} \mathbf{K}_{FF}^{-1}(\mathbf{K}_{FC} \mathbf{a}_C) + \mathbf{K}_{CC} \mathbf{a}_C &= \mathbf{f}_C \\
  \mathbf{K}_{FC}^T \mathbf{K}_{FF}^{-1}(\mathbf{K}_{FC} \mathbf{a}_C) + \mathbf{K}_{CC} \mathbf{a}_C &= \mathbf{f}_C \\
  (\mathbf{K}_{FC}^T \mathbf{K}_{FF}^{-1} \mathbf{K}_{FC} + \mathbf{K}_{CC}) \mathbf{a}_C &= \mathbf{f}_C
\end{align}

Therefore, if \(\mathbf{a}_C(\Delta)\) is halved to \(\mathbf{a}_C(\frac{\Delta}{2})\), then the reaction force must be halved as well since the stiffness term, \(\mathbf{K}_{FC}^T \mathbf{K}_{FF}^{-1} \mathbf{K}_{FC} + \mathbf{K}_{CC}\), in the equation above is independent of the amount of prescribed displacement.

\section{Results \& Discussion}

\subsection{Solution using MATLAB}
The deformed shape of the linear elastic problem presented above is shown in Figure~\ref{fig-MATLABsolution}.

\begin{figure}[!h] 
    \centering
    \includegraphics{imgs/meshedproblemdomain.png}
    \caption{The final converged meshed problem domain.}
    \label{fig-MATLABsolution}
\end{figure}

\subsubsection{Outputs from elem4n that are affected by element displacements ue}
The displacement in the boundary condition, B1, generates an internal force calculated in elem4n as \(f_{int}\) in the nodes existing in the boundary, B1.
This force responds to the displacement in the same direction, y.
While \(f_{ext}\) is not affected by the displacement as it represents the body load which is zero in this problem.
For reaching the equilibrium state, a reaction force is generated which is equal to the internal force in this case.

\subsection{Convergence study}
To determine the convergence with respect to element size, the percent error in the opening force is compared to the converged solution.
To simplify the study, the number of degrees of freedom contained within each mesh is used to represent the element size of each mesh.
Figure~\ref{fig-convergence} shows that for increasing the number of degrees of freedom within the mesh (decreasing element size) the accuracy of the solution in terms of the required opening force start to  converge around approximately 1000 degrees of freedom.

\begin{figure}[!h] 
    \centering
    \includegraphics{imgs/convergence.png}
    \caption{Convergence study of the number of degrees of freedom required to lower the percent error of the required opening force to below 5\%.}
    \label{fig-convergence}
\end{figure}

\subsection{Effective (von Mises) stress distribution}
The von Mises stress distribution is plotted over the deformed shape is shown in Figure~\ref{fig-vonMisesComparison}.

\begin{figure}[!h]
    \centering
    \begin{subfigure}{0.45\textwidth}
    \centering
    \includegraphics[scale=0.5]{imgs/vonMises.png}
    \caption{Computed by MATLAB}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
    \centering
    \includegraphics[scale=0.125]{imgs/vonmises-abaqus.png}
    \caption{Computed by ABAQUS}
    \end{subfigure}
    \caption{Comparison of von Mises stress using both MATLAB and ABAQUS.}
    \label{fig-vonMisesComparison}
\end{figure}

\subsection{Comparison of MATLAB and ABAQUS}

\subsubsection{With respect to the required opening force}
The required opening force was calculated as \(2.25\times10^6 N\) and \(2.20\times10^6 N\) from MATLAB and ABAQUS, respectively.
The slight difference most likely comes from the way our MATLAB script and ABAQUS create the meshed domain with different sizes despite setting the same approximate element sizes.

\subsubsection{With respect to the maximum von Mises stress}
The maximum von Mises stress computed by the MATLAB algorithm and ABAQUS solver are \(350MPa\) and \(556MPa\) which can be seen in Figure~\ref{fig-vonMisesComparison}.
The difference in the maximum value of the von Mises stress arises from ABAQUS using linear interpolation to find the 6 stress component values at each node whereas in the MATLAB routine the average stress in each component was calculated.
As is evident, the method employed by ABAQUS of linear interpolation will be more accurate with respect to the mesh sizes.

In this case, the convergence study (with respect to mesh size) shown above is computed using the required opening force and not the maximum von Mises stress.
The reason that there is not convergence of the maximum von Mises stress for the same mesh size as for the opening force is because the von Mises stress depends on all 6 stress components which all need to converge.

\section{Appendix: Matlab Code}
Main script to run the analysis.

\begin{lstlisting}
% Project 1
% Author: James Koch and Amer Bitar
%--------------------------------------------------------------------------
% PURPOSE
% FE-method to solve the problem of how much force is required to open a
% slit tube. This problem is solved in 2-dimensions with known thickness
% in the third direction.
% 
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

close all; clc;

%% define general parameters and properties
% geometry
douter = 0.5; % m
dinner = 0.3; % m
thickness = 0.5; % m

% material properties
E = 210e9; % Pa
nu = 0.3;

% prescribed displacement
% NOTE: since utilizing symmetry only half of the prescribed displacement
% is used.
delta = -0.006/2; % m

% define approximate element sizes
hr = 0.3; %m
hphi = 0.3; %m

% preallocate result matrix
rB1 = zeros(10, 2);

% initialize error as large
error = 100;
errorvM = 100;
k = 1;

% run solution for various different meshes
while error > 5
    % compute FE-solution for given mesh in each iteration
    [u, r, Ex, Ey, ed, ndof, B1, vonMises] = solfunc(dinner, douter, ...
                                                     thickness, E, nu, ...
                                                     delta, hr, hphi);

    % extract the number of dof for each mesh to be used in the 
    % convergence study
    rB1(k, 1) = ndof;

    % determine the vertical reaction force (y-direction) for ONLY 
    % boundary, B1, corresponding to the opening of a slit in the tube
    rB1(k, 2) = sum(r(B1(:, 2)));

    rB1(k, 3) = hr;
    rB1(k, 4) = hphi;
    
    rB1(k, 5) = max(vonMises);
    
    % compute a percent error of the results in the opening force with respect
    % to the converged solution. The converged solution is the solution where
    % the percent error is less than 5%.
    if k > 1
        error = (abs(rB1(k-1, 2) - rB1(k, 2))/abs(rB1(k, 2)))*100;
        errorvM = (abs(rB1(k-1, 5) - rB1(k, 5))/abs(rB1(k, 5)))*100;
    end
    
    % update approximate element sizes
    hr = hr/2;
    hphi = hphi/2;
    
    % update loop counter
    k = k + 1;
end

%% Plot the converged problem domain
figure(1);
plotpar = [3, 2, 0];
sfac = 50;
eldisp2(Ex, Ey, ed, plotpar, sfac);
xlabel('x');
ylabel('y');
title('Meshed problem domain');
saveas(1, 'imgs/meshedproblemdomain', 'png');

%% Convergence study
% compute a percent error of the results in the opening force with respect
% to the converged solution. The converged solution is the solution where
% the percent error is less than 5%.
rB1(2:end, 6) = (abs(diff(rB1(:, 2)))./abs(rB1(2:end, 2)))*100;
rB1(~any(rB1, 2), :) = [];
rB1(end, :) = [];

% Plot the percent error in the opening force as a function of the number
% of dof for each mesh size.
figure(2);
plot(rB1(2:end, 1), rB1(2:end, 6));
xlabel('Number of degrees of freedom');
ylabel('% error in the opening force');
title('% error in the opening force as a function of the mesh size');
saveas(2, 'imgs/convergence', 'png');

%% Plot von Mises stress
Exd = Ex + sfac*ed(:, 1:2:7);
Eyd = Ey + sfac*ed(:, 2:2:8);

figure(3);
hold on; axis equal;
colorbar;
colormap jet;
fill(Exd', Eyd', vonMises);
xlabel('x');
ylabel('y');
title('von Mises stress in the problem domain');
saveas(3, 'imgs/vonMises1', 'png');
\end{lstlisting}

Function to be able to run multiple meshes to find the converged solution based on the required opening force.

\begin{lstlisting}
function [u, r, Ex, Ey, ed, ndof, B1, vonMises] = solfunc(dinner, douter, thickness, ...
                                              E, nu, delta, hr, hphi)
%function [u, r, ndof, B1, vonMises] = solfunc(dinner, douter, thickness, ...
%                                             E, nu, delta, hr, hphi)
% FE-method to solve the problem of how much force is required to open a
% slit tube. This problem is solved in 2-dimensions with known thickness
% in the third direction.
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% generate the finite element mesh
Ri = dinner;
Ro = douter;
elemtype = 'quad4'; % 4-node rectangular elements

% generate mesh
[Edof, Ex, Ey, B1, ~, B3, ~, ~, ~, ~, ~] = ringmesh(Ri, Ro, elemtype, ...
                                                    hr, hphi);
% define the ndof
ndof = max(Edof(:));

%% compute the element stiffness matrix
% initialize matrices
N = 20;
K = spalloc(ndof, ndof, N*ndof);
f = zeros(size(K, 1), 1);

% define inputs
ue = zeros(1, 8)';
ep = [2, thickness, 2, 1]; 
mp = [E, nu];

n = 9;
% for loop
for i = 1:size(Edof, 1)
    [Ke, fint, fext, ~] = elem4n(ue, Ex(i,:), Ey(i,:), ep, mp);
    K(Edof(i, 2:n), Edof(i, 2:n)) = K(Edof(i, 2:n), Edof(i, 2:n)) + Ke;
    f(Edof(i, 2:n), 1) = f(Edof(i, 2:n), 1) + (fint + fext);
end

%% define the Essential BCs
% symmetry boundary -> B3
bc3 = [B3(1, 1), 0;
       B3(:, 2), zeros(size(B3, 1), 1)];

% prescribed displacement boundary -> B1
bc1 = [B1(:, 2), delta.*ones(size(B1, 1), 1)];
%      B1(:, 1), zeros(size(B1, 1), 1);

bc = [bc1; bc3];

%% solve the FE-system
[u, r] = mysolveq(K, f, bc);
ed = extract(Edof, u);

%% von Mises stress calculation
vonMises = zeros(size(ed, 1), 1);
for i = 1:size(ed, 1)
    ue = ed(i, :)';
    [~, ~, ~, stress] = elem4n(ue, Ex(i,:), Ey(i, :), ep, mp);
    avgstress = mean(stress, 2)';
    vonMises(i, 1) = sqrt(avgstress(1)^2 + avgstress(2)^2 + avgstress(3)^2 + ...
                          avgstress(1)*avgstress(2) + ...
                          avgstress(2)*avgstress(3) + ...
                          avgstress(1)*avgstress(3) + ...
                          3*(avgstress(4)^2 + ...
                             avgstress(5)^2 + ...
                             avgstress(6)^2))/10^6;
end
end
\end{lstlisting}

Function to solve the FE system of equations by partitioning.

\begin{lstlisting}
function [a, Q] = mysolveq(K, f, bc)
%mysolveq

% define free dofs based on which dofs are constrained (i.e. take the
% remaining dofs from total list of dofs
fd = (1:size(K, 1))';
fd(bc(:, 1)) = [];

% partition the stiffness matrix
Kcc = K(bc(:, 1), bc(:,1));  % corresponding to constrained dofs
Kff = K(fd(:, 1), fd(:, 1)); % correspondint to free dofs
Kfc = K(fd(:, 1), bc(:, 1));
Kcf = Kfc';

% define the known displacements
g = bc(:, 2);

% define the known forces
f = f(fd(:, 1));

% solve the system of equations as per the partioned matrix
d = Kff\(f-Kfc*g);
r = Kcc*g + Kcf*d;

a = zeros(size(K, 1), 1);
a(fd(:,1)) = d;
a(bc(:,1)) = bc(:,2);
Q = zeros(size(K, 1), 1);
Q(bc(:,1)) = r;

end
\end{lstlisting}

\end{document}
