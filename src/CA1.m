% Project 1
% Author: James Koch and Amer Bitar
%--------------------------------------------------------------------------
% PURPOSE
% FE-method to solve the problem of how much force is required to open a
% slit tube. This problem is solved in 2-dimensions with known thickness
% in the third direction.
% 
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

close all; clc;

%% define general parameters and properties
% geometry
douter = 0.5; % m
dinner = 0.3; % m
thickness = 0.5; % m

% material properties
E = 210e9; % Pa
nu = 0.3;

% prescribed displacement
% NOTE: since utilizing symmetry only half of the prescribed displacement
% is used.
delta = -0.006/2; % m

% define approximate element sizes
hr = 0.5; %m
hphi = 0.5; %m

% preallocate result matrix
rB1 = zeros(10, 2);

% initialize error as large
error = 100;
errorvM = 100;
k = 1;

% run solution for various different meshes
while error > 5
    % compute FE-solution for given mesh in each iteration
    [u, r, Ex, Ey, ed, ndof, B1, vonMises] = solfunc(dinner, douter, ...
                                                     thickness, E, nu, ...
                                                     delta, hr, hphi);

    % extract the number of dof for each mesh to be used in the 
    % convergence study
    rB1(k, 1) = ndof;

    % determine the vertical reaction force (y-direction) for ONLY 
    % boundary, B1, corresponding to the opening of a slit in the tube
    rB1(k, 2) = sum(r(B1(:, 2)));

    rB1(k, 3) = hr;
    rB1(k, 4) = hphi;
    
    rB1(k, 5) = max(vonMises);
    
    % compute a percent error of the results in the opening force with respect
    % to the converged solution. The converged solution is the solution where
    % the percent error is less than 5%.
    if k > 1
        error = (abs(rB1(k-1, 2) - rB1(k, 2))/abs(rB1(k, 2)))*100;
        errorvM = (abs(rB1(k-1, 5) - rB1(k, 5))/abs(rB1(k, 5)))*100;
    end
    
    % update approximate element sizes
    hr = hr/2;
    hphi = hphi/2;
    
    % update loop counter
    k = k + 1;
end

%% Plot the converged problem domain
figure(1);
plotpar = [3, 2, 0];
sfac = 50;
eldisp2(Ex, Ey, ed, plotpar, sfac);
xlabel('x');
ylabel('y');
title('Meshed problem domain');
saveas(1, 'imgs/meshedproblemdomain', 'png');

%% Convergence study
% compute a percent error of the results in the opening force with respect
% to the converged solution. The converged solution is the solution where
% the percent error is less than 5%.
rB1(2:end, 6) = (abs(diff(rB1(:, 2)))./abs(rB1(2:end, 2)))*100;
rB1(~any(rB1, 2), :) = [];
rB1(end, :) = [];

% Plot the percent error in the opening force as a function of the number
% of dof for each mesh size.
figure(2);
plot(rB1(2:end, 1), rB1(2:end, 6));
xlabel('Number of degrees of freedom');
ylabel('% error in the opening force');
title('% error in the opening force as a function of the mesh size');
saveas(2, 'imgs/convergence', 'png');

%% Plot von Mises stress
Exd = Ex + sfac*ed(:, 1:2:7);
Eyd = Ey + sfac*ed(:, 2:2:8);

figure(3);
hold on; axis equal;
colorbar;
colormap jet;
fill(Exd', Eyd', vonMises);
xlabel('x');
ylabel('y');
title('von Mises stress in the problem domain');
saveas(3, 'imgs/vonMises1', 'png');