function [a, Q] = mysolveq(K, f, bc)
%mysolveq

% define free dofs based on which dofs are constrained (i.e. take the
% remaining dofs from total list of dofs
fd = (1:size(K, 1))';
fd(bc(:, 1)) = [];

% partition the stiffness matrix
Kcc = K(bc(:, 1), bc(:,1));  % corresponding to constrained dofs
Kff = K(fd(:, 1), fd(:, 1)); % correspondint to free dofs
Kfc = K(fd(:, 1), bc(:, 1));
Kcf = Kfc';

% define the known displacements
g = bc(:, 2);

% define the known forces
f = f(fd(:, 1));

% solve the system of equations as per the partioned matrix
d = Kff\(f-Kfc*g);
r = Kcc*g + Kcf*d;

a = zeros(size(K, 1), 1);
a(fd(:,1)) = d;
a(bc(:,1)) = bc(:,2);
Q = zeros(size(K, 1), 1);
Q(bc(:,1)) = r;

end
