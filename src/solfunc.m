function [u, r, Ex, Ey, ed, ndof, B1, vonMises] = solfunc(dinner, douter, thickness, ...
                                              E, nu, delta, hr, hphi)
%function [u, r, ndof, B1, vonMises] = solfunc(dinner, douter, thickness, ...
%                                             E, nu, delta, hr, hphi)
% FE-method to solve the problem of how much force is required to open a
% slit tube. This problem is solved in 2-dimensions with known thickness
% in the third direction.
% 
% Input
%
%
% Output
%
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% generate the finite element mesh
Ri = dinner;
Ro = douter;
elemtype = 'quad4'; % 4-node rectangular elements

% generate mesh
[Edof, Ex, Ey, B1, ~, B3, ~, ~, ~, ~, ~] = ringmesh(Ri, Ro, elemtype, ...
                                                    hr, hphi);
% define the ndof
ndof = max(Edof(:));

%% compute the element stiffness matrix
% initialize matrices
N = 20;
K = spalloc(ndof, ndof, N*ndof);
f = zeros(ndof, 1);

% define inputs
ue = zeros(1, 8)';
ep = [2, thickness, 2, 1]; 
mp = [E, nu];

n = 9;
% for loop
for i = 1:size(Edof, 1)
    [Ke, fint, fext, ~] = elem4n(ue, Ex(i,:), Ey(i,:), ep, mp);
    K(Edof(i, 2:n), Edof(i, 2:n)) = K(Edof(i, 2:n), Edof(i, 2:n)) + Ke;
    f(Edof(i, 2:n), 1) = f(Edof(i, 2:n), 1) + (fint + fext);
end

%% define the Essential BCs
% symmetry boundary -> B3
bc3 = [B3(1, 1), 0;
       B3(:, 2), zeros(size(B3, 1), 1)];

% prescribed displacement boundary -> B1
bc1 = [B1(:, 2), delta.*ones(size(B1, 1), 1)];
%      B1(:, 1), zeros(size(B1, 1), 1);

bc = [bc1; bc3];

%% solve the FE-system
[u, r] = mysolveq(K, f, bc);
ed = extract(Edof, u);

%% von Mises stress calculation
vonMises = zeros(size(ed, 1), 1);
for i = 1:size(ed, 1)
    ue = ed(i, :)';
    [~, ~, ~, stress] = elem4n(ue, Ex(i,:), Ey(i, :), ep, mp);
    avgstress = mean(stress, 2)';
    vonMises(i, 1) = sqrt(avgstress(1)^2 + avgstress(2)^2 + avgstress(3)^2 + ...
                          avgstress(1)*avgstress(2) + ...
                          avgstress(2)*avgstress(3) + ...
                          avgstress(1)*avgstress(3) + ...
                          3*(avgstress(4)^2 + ...
                             avgstress(5)^2 + ...
                             avgstress(6)^2))/10^6;
end
end