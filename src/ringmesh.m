function [Edof,Ex,Ey,B1,B2,B3,B4,P1,P2,P3,P4]=ringmesh(Ri,Ro,elemtype,hr,hphi)
%[Edof,Ex,Ey,B1,B2,B3,B4,P1,P2,P3,P4]=ringmesh(Ri,Ro,elemtype,hr,hphi)
%
%Purpose:
% Generates a mesh for a two-dimensional half of a ring-shaped domain
%
%Input:
% Ri    - Inner radius of the ring
% Ro    - Outer radius of the ring
% elemtype - Element type    "tria3"  - 3-node triangular element
%                            "quad4" - 4-node quadrilateral element
% hr    - Approximate element size in radial direction
% hphi  - Approximate element size in circumferencial direction
%
%Output:
% Edof  - Connectivity matrix for mesh, cf. Calfem Toolbox
% Ex    - Elementwise x-coordinates, cf. Calfem Toolbox
% Ey    - Elementwise y-coordinates, cf. Calfem Toolbox
% Bi    - Matrix containing x-dofs (1st column) and y-dofs (2nd column)
%         for nodes on boundary segment i (i=1,2,3,4)
% Pi    - Rowvector containing x-dof (1st value) and y-dof (2nd value)
%         for corner node i (i=1,2,3,4)
%
%Written by
% Fredrik Larsson, November 27, 2008   - Original version
% Fredrik Larsson, December  1, 2008   - Updated due to error in Edof 
% Knut A Meyer, January 25, 2017       - Fixed plotting part

nelr=max(ceil((Ro-Ri)/hr),1);
nelphi=max(ceil((pi*(Ro+Ri)/2)/hphi),2);

if elemtype=='tria3';
    for i=1:nelphi
        for j=1:nelr
            sw=(i-1).*(nelr+1)+j;
            nw=sw+1;
            se=sw+(nelr+1);
            ne=se+1;
            elno=2*(i-1)*nelr+2*(j-1);
            Edof(elno+1,:)=[elno+1,2*sw-1,2*sw,2*se-1,2*se,2*nw-1,2*nw];
            Edof(elno+2,:)=[elno+2,2*se-1,2*se,2*ne-1,2*ne,2*nw-1,2*nw];
            phiw=pi.*((i-1)/nelphi);
            phie=pi.*(i/nelphi);
            rs=Ri+(Ro-Ri)*((j-1)/nelr);
            rn=Ri+(Ro-Ri)*(j/nelr);
            Ex(elno+1,:)=[rs.*cos(phiw) rs.*cos(phie) rn.*cos(phiw)];
            Ey(elno+1,:)=-[rs.*sin(phiw) rs.*sin(phie) rn.*sin(phiw)];
            Ex(elno+2,:)=[rs.*cos(phie) rn.*cos(phie) rn.*cos(phiw)];
            Ey(elno+2,:)=-[rs.*sin(phie) rn.*sin(phie) rn.*sin(phiw)];
        end
    end
elseif elemtype=='quad4';
    for i=1:nelphi
        for j=1:nelr
            sw=(i-1).*(nelr+1)+j;
            nw=sw+1;
            se=sw+(nelr+1);
            ne=se+1;
            elno=(i-1)*nelr+(j-1);
            Edof(elno+1,:)=[elno+1,2*sw-1,2*sw,2*se-1,2*se,2*ne-1,2*ne,2*nw-1,2*nw];
            phiw=pi.*((i-1)/nelphi);
            phie=pi.*(i/nelphi);
            rs=Ri+(Ro-Ri)*((j-1)/nelr);
            rn=Ri+(Ro-Ri)*(j/nelr);
            Ex(elno+1,:)=[rs.*cos(phiw) rs.*cos(phie) rn.*cos(phie) rn.*cos(phiw)];
            Ey(elno+1,:)=-[rs.*sin(phiw) rs.*sin(phie) rn.*sin(phie) rn.*sin(phiw)];
        end
    end
end

figure(1); clf;
hold on;
for i=1:length(Edof(:,1));
    plot(Ex(i,[1:end 1]),Ey(i,[1:end 1]), 'k');
end
text(1.05.*Ro,0,'P1');
text(Ri-0.15.*Ro,0,'P2');
text((Ri+Ro)/2,Ro/10,'B1');
text(0,-Ri+Ro/10,'B2');
text(0,-Ro-Ro/10,'B4');
text(-1.15.*Ro,0,'P4')
text(-Ri+0.05*Ro,0,'P3')
text(-(Ri+Ro)/2,Ro/10,'B3');

axis([-1.2 1.2 -1.2 0.2]*Ro) 
axis equal

B1=[1:(nelr+1)]';
B1=[2.*B1-1,2*B1];
B2=[1:(nelr+1):(nelphi*(nelr+1)+1)]';
B2=[2.*B2-1,2*B2];
B3=[nelphi*(nelr+1)+(1:(nelr+1))]';
B3=[2.*B3-1,2.*B3];
B4=(nelr+1)*(nelphi+1)+1-[1:(nelr+1):(nelphi*(nelr+1)+1)]';
B4=[2.*B4-1,2*B4];

P1=B1(end,:);
P2=B1(1,:);
P3=B2(end,:);
P4=B4(1,:);

